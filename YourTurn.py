import sys, os, random
from operator import itemgetter

#Party Template, names must be unique - ["name", modifier, 0, 0]
#example - [["Bill", 0, 0, 0], ["Bob", -3, 0, 0], ["Jeb", 5, 0, 0]]
template = [["Bill", 0, 0, 0], ["Bob", -3, 0, 0], ["Jeb", 5, 0, 0]]

def header():							#Graphical Header
	print(" ---------- YourTurn ----------\n ")

def cont():								#Function to continue
	input("\nPress Return to continue...")

def fail():								#Fail Checker
	print("You fucked up Shitlord.")
	cont()

def clear_screen():						#Clears the screen - OS specific
	if sys.platform == 'linux2' or sys.platform == 'linux':
		os.system('clear')
	elif sys.platform == 'darwin':
		os.system('clear')
	elif sys.platform == 'win32':
		os.system('cls')
	else:
		print("\n") * 5

def int_check(user_input):				#Check user input is an integer
	check = user_input.lstrip("-").isdigit()
	return check

def dice(a):							#Rolls The Dice
	total = 0
	number_of_dice = a[2]
	while number_of_dice >= 1:
		roll = random.randint(a[0], a[1])
		number_of_dice = number_of_dice - 1
		total = total + roll
	return total

def close():							#Close YourTurn
	clear_screen()
	header()
	print("Thank you for using YourTurn!")
	cont()
	clear_screen()
	quit()

def init_system():						#Define the initative dice
	clear_screen()
	header()
	print("1:) 1D20 - Dungeons & Dragons.")
	print("2:) 1D8 - Stars without number.")
	print("3:) 1D6 - Shadowrun.")
	choice = input("\nPlease select your initiative system.\n> ")
	if choice == "1":
		dice = [1, 20, 1]
	elif choice == "2":
		dice = [1, 8, 1]
	elif choice == "3":
		dice = [1, 6, 1]
	else:
		fail()
		dice = init_system()
	return dice

def sort(a):							#Sort the list
	sort_list = sorted(a, key=itemgetter(3), reverse=True)
	return sort_list

def menu1(entities):					#Main Menu
	clear_screen()
	header()
	print("1: Load the party template.")
	print("2: Add entries to the initiative table.")
	print("3: Display the table contents.")
	print("4: Edit the table contents.")
	print("5: Wipe the table.")
	print("6: Generate Initiative.")
	print("7: Exit YourTurn.")
	choice = input("\nWhat would you like to do?\n> ")
	if choice == "1":
		for i in template:
			entities.append(i)
		clear_screen()
		header()
		print("Template loaded.")
		cont()
		menu1(entities)
	elif choice == "2":
		clear_screen()
		header()
		more = "y"
		while more == "y" or more == "Y":
			input_list = []
			name = input("Player name: > ")
			mod = input("Enter initiative modifier: > ")
			check = int_check(mod)
			if check == True:
				mod = int(mod)
			else:
				fail()
				menu1(entities)
			input_list.append(name)
			input_list.append(mod)
			input_list.append(0)
			input_list.append(0)
			entities.append(input_list)
			more = input("Would you like to add more enteries? (y/n) > ")
		menu1(entities)
	elif choice == "3":
		clear_screen()
		header()
		for i in entities:
			print("%s %d" % (i[0], i[1]))
		cont()
		menu1(entities)
	elif choice == "4":
		clear_screen()
		header()
		for i in entities:
			print(i[0])
		choice_name = input("\nWho do you want to edit?\n> ")
		clear_screen()
		header()
		print("1: Delete the entry.")
		print("2: Change the modifier.")
		choice = input("\nWhat would you like to do?\n> ")
		if choice == "1":
			new_entities = []
			for i in entities:
				if choice_name != i[0]:
					  new_entities.append(i)
			entieis = new_entities
			menu1(entieis)
		elif choice == "2":
			clear_screen()
			header()
			new_mod = input("What is the new modifier? > ")
			choice = int_check(new_mod)
			if choice == True:
				new_mod = int(new_mod)
			else:
				fail()
				menue1(entites)
			for i in entities:
				if choice_name == i[0]:
					i[1] = new_mod
			menu1(entities)
	elif choice == "5":
		entities = []
		clear_screen()
		header()
		print("Table wiped!!!")
		cont()
		menu1(entities)
	elif choice == "6":
		clear_screen()
		header()
		initiative_table = generate_initiative(entities, init_dice)
		menu2(initiative_table, entities)
	elif choice == "7":
		close()
	else:
		clear_screen()
		header()
		fail()
		menu1(entities)

def menu2(initiative_table, entities):	#Initiative Table Munu
	clear_screen()
	header()
	sorted_table = sort(initiative_table)
	for i in sorted_table:
		print("%s %d" % (i[0], i[3]))
	cont()
	clear_screen()
	header()
	print("1: Edit an Entry")
	print("2: Display the table.")
	print("3: Return to the main menu.")
	choice = input("\nWhat would you like to do?\n> ")
	if choice == "1":
		clear_screen()
		header()
		print("Name - Modifier - Roll - Result\n")
		for i in initiative_table:
			print("%s %d %d %d" % (i[0], i[1], i[2], i[3]))
		choice_name = input("\nWho do you want to edit?\n> ")
		print("\n1: Delete the entry.")
		print("2: Change initiative.")
		choice = input("\nWhat would you like to do?\n> ")
		if choice == "1":
			new_table = []
			for i in initiative_table:
				if choice_name != i[0]:
					  new_table.append(i)
			initiative_table = new_table
			menu2(initiative_table, entities)
		elif choice == "2":
			new_mod = input("\nChange initiative by?\n> ")
			choice = int_check(new_mod)
			if choice == True:
				new_mod = int(new_mod)
			else:
				fail()
				menue2(initiative_table, entites)
			for i in initiative_table:
				if choice_name == i[0]:
					i[2] = i[2] + new_mod
					i[3] = i[1] + i[2]
			menu2(initiative_table, entities)
	elif choice == "2":
		clear_screen()
		header()
		menu2(initiative_table, entities)
	elif choice == "3":
		menu1(entities)
	else:
		fail()
		menu2(initiative_table, entities)

def generate_initiative(a, b):			#Roll Initiative and add modifier
	for i in a:
		i[2] = dice(b)
		i[3] = i[1] + i[2]
	return a

init_dice = init_system()
entities = []
menu1(entities)
